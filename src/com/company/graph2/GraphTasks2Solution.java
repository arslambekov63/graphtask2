package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    public HashSet<Integer> visitedVertexes = new HashSet<>();
    ArrayDeque<Integer> dequeToVisit = new ArrayDeque<>();
    int[][] adjacencyMatrix;
    HashMap<Integer, Integer> map = new HashMap<>();

    private List<HashSet<Integer>> connectivityComponents = new ArrayList<>();
    private int maxEdgeSize;





    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        this.adjacencyMatrix = adjacencyMatrix;

        for (int i =0; i< adjacencyMatrix.length; i++) {
            map.put(i,Integer.MAX_VALUE);
        }
        map.put(startIndex, 0);
        dequeToVisit.add(startIndex);
        boolean flag = false;
        while (!flag) {
            if (dequeToVisit.peek() != null) {
                int vertex = dequeToVisit.poll();
                checkVertex(vertex);
            } else {
                flag = true;
            }
        }
        visitedVertexes.clear();
        return map;
    }
    private void checkVertex(int vertex) {
        if (!visitedVertexes.contains(vertex)) {
            List<Integer> children = addAllChildrenToDeque(vertex);
            for (Integer integer : children) {
                //тут сравнить метки
                if (map.get(integer) > map.get(vertex) + adjacencyMatrix[vertex][integer])
                {
                    map.put(integer, map.get(vertex) + adjacencyMatrix[vertex][integer]);
                }
            }
        }
        visitedVertexes.add(vertex);
    }
    private List<Integer> addAllChildrenToDeque(int vertex) {
        List<Integer> childrenList = new ArrayList<>();
        for (int i=0; i< adjacencyMatrix[vertex].length; i++) {
            if (adjacencyMatrix[vertex][i] != 0 & !visitedVertexes.contains(i)) {
                dequeToVisit.add(i);
                childrenList.add(i);
            }
        }
        return childrenList;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int size = 0;
        this.adjacencyMatrix = adjacencyMatrix;
        //Пройтись по матрице и найти ребро, такое что
        //во первых оно не соединяет с посещенным ребром
        //во вторых оно минимально
        visitedVertexes.add(1);
        boolean flag = false;
        while (!flag) {
            int minEdgeSize = Integer.MAX_VALUE;
            int minEdgeVertex = 0;
            for (Integer integer : visitedVertexes) {
                int[] vertexAndEdgeSize = checkAllEdges(integer);
                if (vertexAndEdgeSize[1] < minEdgeSize) {
                    minEdgeVertex = vertexAndEdgeSize[0];
                    minEdgeSize = vertexAndEdgeSize[1];
                }
            }
            if (minEdgeVertex != 0) {
                visitedVertexes.add(minEdgeVertex);
                size += minEdgeSize;
            }
            if (visitedVertexes.size() == adjacencyMatrix.length) {
                flag = true;
            }
        }
        visitedVertexes.clear();
        return size;
    }

    private int[] checkAllEdges(int vertex) {
        int minEdgeSize = Integer.MAX_VALUE;
        int minEdgeVertex = 0;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[vertex-1][i] < minEdgeSize && adjacencyMatrix[vertex-1][i] !=0 & !visitedVertexes.contains(i+1)) {
                minEdgeSize = adjacencyMatrix[vertex-1][i];
                minEdgeVertex = i+1;
            }
        }
        return  new int[] {minEdgeVertex, minEdgeSize};
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        maxEdgeSize = 0;
        int answer = 0;
        Map<Integer, List<Pair>> edges = generateMap(adjacencyMatrix);
        for (int i = 0; i < maxEdgeSize; i++) {
            List<Pair> edgesWithConcreteSize = edges.get(i);
            if (edgesWithConcreteSize != null) {
                for (Pair pair : edgesWithConcreteSize) {
                    HashSet<Integer> connectivityComponentWithFirstVertex = null;
                    HashSet<Integer> connectivityComponentWithLastVertex = null;
                    //тут надо две вершины проверить на принадлежность к комп связности
                    for (HashSet<Integer> connectivityComponent : connectivityComponents) {
                        //пробежаться по комп связности и определить принадлежность к верш
                        if (connectivityComponent.contains(pair.firstDigit))
                            connectivityComponentWithFirstVertex = connectivityComponent;
                        if (connectivityComponent.contains(pair.lastDigit))
                            connectivityComponentWithLastVertex = connectivityComponent;
                    }
                    //тут обработать результаты для одной пары
                    if (connectivityComponentWithFirstVertex == connectivityComponentWithLastVertex && connectivityComponentWithFirstVertex != null) {
                        continue; // случай когда вершины из одной комп связности
                    }

                    if (connectivityComponentWithFirstVertex == null && connectivityComponentWithLastVertex != null) {
                        connectivityComponentWithLastVertex.add(pair.firstDigit);
                        answer += adjacencyMatrix[pair.firstDigit - 1][pair.lastDigit - 1];
                        continue;
                        //случай когда одна вершина из комп связности, другая не содержится ни в одном 1
                    }
                    if (connectivityComponentWithLastVertex == null && connectivityComponentWithFirstVertex != null) {
                        connectivityComponentWithFirstVertex.add(pair.lastDigit);
                        answer += adjacencyMatrix[pair.firstDigit - 1][pair.lastDigit - 1];
                        continue;
                        //случай когда одна вершина из комп связности, другая не содержится ни в одном 2
                    }


                    if (connectivityComponentWithFirstVertex == null && connectivityComponentWithLastVertex == null) {
                        HashSet<Integer> newComponentConnectivity = new HashSet<>();
                        newComponentConnectivity.add(pair.firstDigit);
                        newComponentConnectivity.add(pair.lastDigit);
                        connectivityComponents.add(newComponentConnectivity);
                        answer += adjacencyMatrix[pair.firstDigit - 1][pair.lastDigit - 1];
                        continue;
                        //вершины не содержатся в комп связности. В таком случае они образуют новую
                    } else {

                        connectivityComponentWithFirstVertex.addAll(connectivityComponentWithLastVertex);
                        connectivityComponents.remove(connectivityComponentWithLastVertex);
                        answer += adjacencyMatrix[pair.firstDigit - 1][pair.lastDigit - 1];
                        continue;
                        //вершины в разных компонентах связности, в таком случае объеденить вершины
                    }
                }
            }
        }
        connectivityComponents.clear();
        return answer;
    }
    private Map<Integer, List<Pair>> generateMap(int[][] adjacencyMatrix) {
        //сначала создать списки в мапе
        //еще найти макс размер ребра
        Map<Integer, List<Pair>> result = new HashMap<>();
        int maxEdgeSize = 0;
        for (int i = 0; i< adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (result.get(adjacencyMatrix[i][j]) == null) {
                    List<Pair> edge = new ArrayList<>();
                    result.put(adjacencyMatrix[i][j], edge);
                }
                //доп проверка
                if (adjacencyMatrix[i][j] != Integer.MIN_VALUE && adjacencyMatrix[i][j] !=0) {
                    List<Pair> edgeListFromMap = result.get(adjacencyMatrix[i][j]);
                    edgeListFromMap.add(new Pair(i+1, j+1));
                    if (adjacencyMatrix[i][j] > maxEdgeSize)
                        maxEdgeSize = adjacencyMatrix[i][j];
                    adjacencyMatrix[j][i] = Integer.MIN_VALUE;
                }
            }
        }
        this.maxEdgeSize = maxEdgeSize;
        return result;
    }
}
