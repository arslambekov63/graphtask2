package com.company.graph2;

public class Pair {
    int firstDigit;
    int lastDigit;
    public void setPair(int firstDigit, int lastDigit) {
        this.firstDigit = firstDigit;
        this.lastDigit = lastDigit;
    }

    public int[] getPair() {
        return  new int[]{firstDigit, lastDigit};
    }

    public Pair(int firstDigit, int lastDigit) {
        this.firstDigit = firstDigit;
        this.lastDigit = lastDigit;
    }
}
